# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.5.2](https://gitlab.com/Atomizer99/npm-chnlg/compare/v1.5.0...v1.5.2) (2022-05-16)

### [1.5.1](https://gitlab.com/Atomizer99/npm-chnlg/compare/v1.5.0...v1.5.1) (2022-05-16)

## [1.5.0](https://gitlab.com/Atomizer99/npm-chnlg/compare/v1.4.0...v1.5.0) (2022-05-16)

### [0.1.2](https://gitlab.com/Atomizer99/npm-chnlg/compare/v0.1.0...v0.1.2) (2022-05-16)

### [0.1.1](https://gitlab.com/Atomizer99/npm-chnlg/compare/v0.1.0...v0.1.1) (2022-05-16)

### [0.0.8](https://gitlab.com/Atomizer99/npm-chnlg/compare/v0.0.7...v0.0.8) (2022-05-16)

### [0.0.7](https://gitlab.com/Atomizer99/npm-chnlg/compare/v0.0.6...v0.0.7) (2022-04-24)


### Features

* squash commit message txt file ([cf03431](https://gitlab.com/Atomizer99/npm-chnlg/commit/cf03431a9aafdf7c7a7ba25b56fc1432d2348cfa))


### Bug Fixes

* **txt:** done with txt ([35dfabf](https://gitlab.com/Atomizer99/npm-chnlg/commit/35dfabfe39f0ff9e9f7e0f67fc3222744fa7932d))
* **txt:** merge commit message txt file solved ([597f5a7](https://gitlab.com/Atomizer99/npm-chnlg/commit/597f5a7319353255b30630805cd60147a75ee9ae))

### [0.0.6](https://gitlab.com/Atomizer99/npm-chnlg/compare/v0.0.5...v0.0.6) (2022-04-24)

### [0.0.5](https://gitlab.com/Atomizer99/npm-chnlg/compare/v0.0.4...v0.0.5) (2022-04-24)


### Features

* added changelog ([bf2d8cb](https://gitlab.com/Atomizer99/npm-chnlg/commit/bf2d8cb3b7aed3bdda0ccd9cac66a3f817d3490a))

### 0.0.4 (2022-04-24)


### Bug Fixes

* **changelog:** added auto-changelog pakcage to package.json ([9daaac0](https://gitlab.com/Atomizer99/npm-chnlg/commit/9daaac096e35088cee1bbd43b42813d4ef437c94))
